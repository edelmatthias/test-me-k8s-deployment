from flask import Flask
from kubernetes import client, config
app = Flask(__name__)

@app.route("/")
def hello():
    #TODO : maybe take that to a configuration (development vs pod running in k8s cluster)
    try:
        # to be used when inside a kubernetes cluster
        config.load_incluster_config()
    except:
        try:
            # use .kube directory
            # for local development
            config.load_kube_config()
        except:
            print("No kubernetes cluster defined")

    #use .kube directory
    #config.load_kube_config()

    v1 = client.CoreV1Api()
    text = "<b>Listing pods in memobase namespace</b><br>"
    #ret = v1.list_pod_for_all_namespaces(watch=False)
    ret = v1.list_namespaced_pod(namespace="memobase")
    for i in ret.items:
        text=text+i.metadata.namespace+'/'+i.metadata.name+'<br>'
    return text

if __name__ == "__main__":
    app.run(host='0.0.0.0')
